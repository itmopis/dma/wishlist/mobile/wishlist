
package com.manama.wishlist.data

import com.manama.wishlist.core.datastore.preferences.UserPreferences
import com.manama.wishlist.domain.repository.UserPreferenceRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UserPreferenceRepositoryImpl @Inject constructor(
    private val preferences: com.manama.wishlist.core.datastore.preferences.UserPreferences

) : UserPreferenceRepository {
    override suspend fun setTheme(themeValue: String) {
        return preferences.setTheme(themeValue = themeValue)
    }

    override fun getTheme(): Flow<String> {
        return preferences.getTheme()
    }

    override suspend fun setIsFirstTimeLaunch() {
        return preferences.setIsFirstTimeLaunch()
    }

    override fun getIsFirstTimeLaunch(): Flow<Boolean> {
        return preferences.getIsFirstTimeLaunch()
    }

}