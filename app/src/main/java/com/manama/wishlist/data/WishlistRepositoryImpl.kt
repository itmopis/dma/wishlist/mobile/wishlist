
package com.manama.wishlist.data

import com.manama.wishlist.core.models.WishListItem
import com.manama.wishlist.core.room.database.WishlistAppDatabase
import com.manama.wishlist.core.room.entities.WishlistItemEntity
import com.manama.wishlist.core.util.toEntity
import com.manama.wishlist.domain.repository.WishlistRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WishlistRepositoryImpl @Inject constructor(
    private val db: WishlistAppDatabase
) : WishlistRepository {
    override fun getAllWishlistItems(): Flow<List<WishlistItemEntity>> {
        return db.wishlistItemEntityDao.getAllWishlistItems()
    }

    override suspend fun insertWishListItem(item: WishListItem) {
        return db.wishlistItemEntityDao.insertWishlistItem(wishlistItemEntity = item.toEntity())
    }

    override suspend fun getWishListItemById(itemId: String): WishlistItemEntity? {
        return db.wishlistItemEntityDao.getWishListItemById(itemId = itemId)
    }

    override suspend fun deleteWishListItemById(itemId: String) {
        return db.wishlistItemEntityDao.deleteWishListItemById(itemId = itemId)
    }

    override suspend fun updateWishListItemById(
        itemId: String,
        name: String,
        amount: Int,
        quantity: Int,
        category: String,
        priority: String
    ) {
        return db.wishlistItemEntityDao.updateWishListItemById(
            itemId = itemId, name = name,
            amount = amount, quantity = quantity,
            category = category, priority = priority
        )
    }
}