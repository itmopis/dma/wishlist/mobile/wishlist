
package com.manama.wishlist.domain.repository

import com.manama.wishlist.core.models.WishListItem
import com.manama.wishlist.core.room.entities.WishlistItemEntity
import kotlinx.coroutines.flow.Flow

interface WishlistRepository {

    fun getAllWishlistItems(): Flow<List<WishlistItemEntity>>

    suspend fun insertWishListItem(item: WishListItem)

    suspend fun getWishListItemById(itemId: String): WishlistItemEntity?

    suspend fun deleteWishListItemById(itemId: String)

    suspend fun updateWishListItemById(
        itemId: String, name: String, amount: Int,
        quantity: Int, category: String, priority: String
    )


}