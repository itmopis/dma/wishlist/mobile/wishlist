
package com.manama.wishlist.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    //background
    background = BLACK_COLOR,
    onBackground = BLACK_COLOR_VARIANT,

    //text
    primary = WHITE_COLOR_VARIANT,
    onPrimary = WHITE_COLOR,

    surface = BLUE_COLOR,
)

private val LightColorPalette = lightColors(
    background = WHITE_COLOR_VARIANT,
    onBackground = WHITE_COLOR,

    //text
    primary = BLACK_COLOR,
    onPrimary = BLACK_COLOR_VARIANT,

    surface = BLUE_COLOR,

    )

@Composable
fun MyWishListAppTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}