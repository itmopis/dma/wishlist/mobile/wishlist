
package com.manama.wishlist.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.compose.rememberNavController
import com.manama.wishlist.core.util.Constants
import com.manama.wishlist.ui.navigation.AppNavigation
import com.manama.wishlist.ui.theme.MyWishListAppTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel: MainViewModel = hiltViewModel()
            val theme = viewModel.theme
                .collectAsStateWithLifecycle(
                    initialValue = Constants.DARK_MODE,
                    context = Dispatchers.Main.immediate
                )

            MyWishListAppTheme(
                darkTheme = theme.value == Constants.DARK_MODE
            ) {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    val navController = rememberNavController()
                    AppNavigation(navHostController = navController)
                }
            }
        }
    }
}