
package com.manama.wishlist.ui.screens.onboarding

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manama.wishlist.core.util.Screens
import com.manama.wishlist.core.util.UiEvent
import com.manama.wishlist.domain.repository.UserPreferenceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class OnboardingScreenViewModel @Inject constructor(
    private val repository: UserPreferenceRepository
) : ViewModel() {


    private val _eventFlow = MutableSharedFlow<UiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()


    fun onProceed() {
        viewModelScope.launch {
            _eventFlow.emit(UiEvent.Navigate(Screens.BOTTOM_TAB_NAVIGATION_WRAPPER))
            repository.setIsFirstTimeLaunch()
        }
    }

}