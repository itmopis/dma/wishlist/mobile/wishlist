
package com.manama.wishlist.ui

import androidx.lifecycle.ViewModel
import com.manama.wishlist.domain.repository.UserPreferenceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val userPreferencesRepository: UserPreferenceRepository
) : ViewModel() {

    val theme = userPreferencesRepository.getTheme()


}