
package com.manama.wishlist.ui.screens.settings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.manama.wishlist.domain.repository.UserPreferenceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsScreenViewModel @Inject constructor(
    private val userPreferenceRepository: UserPreferenceRepository
) : ViewModel() {

    val theme = userPreferenceRepository.getTheme()

    fun updateTheme(themeValue: String) {
        viewModelScope.launch {
            userPreferenceRepository.setTheme(themeValue = themeValue)

        }
    }
}