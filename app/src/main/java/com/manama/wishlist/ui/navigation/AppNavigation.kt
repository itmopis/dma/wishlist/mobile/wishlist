
package com.manama.wishlist.ui.navigation

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.manama.wishlist.core.util.Screens
import com.manama.wishlist.ui.screens.onboarding.OnboardingScreen
import com.manama.wishlist.ui.screens.wishlist_item.WishListItemScreen

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun AppNavigation(
    navHostController: NavHostController,
    viewModel: AppNavigationViewModel = hiltViewModel()
) {
    val isFirstTimeLaunch = viewModel.isFirstTimeLaunch.collectAsStateWithLifecycle().value
    val activity = (LocalContext.current as? Activity)
    LaunchedEffect(key1 = isFirstTimeLaunch) {
        if (!isFirstTimeLaunch) {
            navHostController.navigate(Screens.BOTTOM_TAB_NAVIGATION_WRAPPER)
        }
    }
    NavHost(
        navController = navHostController,
        startDestination = Screens.ONBOARDING_SCREEN,
    ) {
        composable(route = Screens.ONBOARDING_SCREEN) {
            OnboardingScreen(navController = navHostController)
        }
        composable(route = Screens.WISHLIST_ITEM_SCREEN + "/{id}") {
            WishListItemScreen(navController = navHostController)
        }
        composable(route = Screens.BOTTOM_TAB_NAVIGATION_WRAPPER) {
            BackHandler(enabled = true) {
                activity?.finish()

            }
            BottomTabNavigationWrapper(navHostController = navHostController)
        }
    }
}