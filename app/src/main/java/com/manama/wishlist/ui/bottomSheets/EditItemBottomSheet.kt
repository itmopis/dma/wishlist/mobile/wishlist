
package com.manama.wishlist.ui.bottomSheets

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.manama.wishlist.core.util.Constants
import com.manama.wishlist.core.util.Constants.categoryList
import com.manama.wishlist.core.util.Constants.priorityList
import com.manama.wishlist.core.util.isNumeric
import com.manama.wishlist.ui.components.MenuSample
import com.manama.wishlist.ui.screens.wishlist_item.WishlistItemScreenViewModel

@Composable
fun EditItemBottomSheet(
    viewModel: WishlistItemScreenViewModel = hiltViewModel()

) {
    Column(
        modifier = Modifier
            .height(500.dp)
            .padding(16.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        viewModel.editWishListItem.value?.let { wishlistItem ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = "Add a new wishlist Item ",
                    fontWeight = FontWeight.Bold,
                    style = TextStyle(color = MaterialTheme.colors.primary)
                )
            }
            TextField(
                modifier = Modifier.fillMaxWidth(),
                value = wishlistItem.name,
                onValueChange = {
                    viewModel._editWishListItem.value = wishlistItem.copy(name = it)

                },
                label = {
                    Text(
                        text = "Name",
                        style = TextStyle(color = MaterialTheme.colors.primary)
                    )
                },

                colors = TextFieldDefaults.textFieldColors(
                    textColor = MaterialTheme.colors.primary,
                    focusedIndicatorColor = MaterialTheme.colors.surface,

                    ),
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(100.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly,
            ) {
                TextField(
                    modifier = Modifier.width(200.dp),
                    value = wishlistItem.amount.toString(),
                    onValueChange = { text ->
                        if (text.isBlank()) {
                            viewModel._editWishListItem.value = wishlistItem.copy(amount = 0)
                            return@TextField
                        }
                        if (isNumeric(text)) {
                            viewModel._editWishListItem.value =
                                wishlistItem.copy(amount = text.toInt())

                        }


                    },
                    label = {
                        Text(
                            text = "Amount",
                            style = TextStyle(color = MaterialTheme.colors.primary)
                        )
                    },

                    colors = TextFieldDefaults.textFieldColors(
                        textColor = MaterialTheme.colors.primary,
                        focusedIndicatorColor = MaterialTheme.colors.surface,

                        ),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                    ),
                )
                Spacer(
                    modifier = Modifier
                        .width(10.dp)

                )
                TextField(
                    modifier = Modifier.width(200.dp),
                    value = wishlistItem.quantity.toString(),
                    onValueChange = { text ->
                        if (text.isBlank()) {
                            viewModel._editWishListItem.value = wishlistItem.copy(quantity = 0)
                            return@TextField
                        }
                        if (isNumeric(text)) {
                            viewModel._editWishListItem.value =
                                wishlistItem.copy(quantity = text.toInt())

                        }
                    },
                    label = {
                        Text(
                            text = "Quantity",
                            style = TextStyle(color = MaterialTheme.colors.primary)
                        )
                    },
                    colors = TextFieldDefaults.textFieldColors(
                        textColor = MaterialTheme.colors.primary,
                        focusedIndicatorColor = MaterialTheme.colors.surface,

                        ),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                    ),
                )

            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(100.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly,
            ) {
                Column() {
                    Text(
                        text = "Category",
                        style = TextStyle(color = MaterialTheme.colors.primary)
                    )

                    MenuSample(
                        selectedIndex = categoryList.indexOf(wishlistItem.category),
                        onChangeSelectedIndex = {
                            viewModel._editWishListItem.value =
                                wishlistItem.copy(category = categoryList[it])
                        },
                        menuItems = categoryList,
                        menuWidth = 200,
                    )
                }
                Column() {
                    Text(
                        text = "Priority",
                        style = TextStyle(color = MaterialTheme.colors.primary)
                    )
                    MenuSample(
                        selectedIndex = priorityList.indexOf(wishlistItem.priority),
                        onChangeSelectedIndex = {
                            viewModel._editWishListItem.value =
                                wishlistItem.copy(priority = priorityList[it])
                        },
                        menuItems = Constants.priorityList,
                        menuWidth = 200
                    )

                }

            }
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
                    .clip(RoundedCornerShape(10.dp)),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = MaterialTheme.colors.surface
                ),
                onClick = {
                    viewModel.editItem()
                }
            ) {
                Text(
                    text = "Save Changes",
                    fontWeight = FontWeight.Bold,
                    style = TextStyle(
                        color = MaterialTheme.colors.primary
                    )
                )

            }
        }

    }
}