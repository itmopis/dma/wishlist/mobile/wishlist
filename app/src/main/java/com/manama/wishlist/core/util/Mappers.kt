
package com.manama.wishlist.core.util

import com.manama.wishlist.core.models.WishListItem
import com.manama.wishlist.core.room.entities.WishlistItemEntity


fun WishListItem.toEntity(): WishlistItemEntity {
    return WishlistItemEntity(
        itemId = itemId,
        name = name,
        amount = amount,
        quantity = quantity,
        category = category,
        priority = priority,
        createdAt = createdAt,
        imageUrl = imageUrl,
        isPurchased = isPurchased
    )

}

fun WishlistItemEntity.toExternalModel(): WishListItem {
    return WishListItem(
        itemId = itemId,
        name = name,
        amount = amount,
        quantity = quantity,
        category = category,
        priority = priority,
        createdAt = createdAt,
        imageUrl = imageUrl,
        isPurchased = isPurchased
    )
}