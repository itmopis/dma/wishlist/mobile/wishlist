
package com.manama.wishlist.core.room.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.manama.wishlist.core.room.dao.WishListItemEntityDao
import com.manama.wishlist.core.room.entities.WishlistItemEntity

@Database(
    entities = [
        WishlistItemEntity::class,

    ],
    version = 1,
    exportSchema = true
)
abstract class WishlistAppDatabase : RoomDatabase() {

    abstract val wishlistItemEntityDao: WishListItemEntityDao


}