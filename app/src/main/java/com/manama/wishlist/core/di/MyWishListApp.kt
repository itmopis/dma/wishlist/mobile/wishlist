
package com.manama.wishlist.core.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class MyWishListApp : Application()