
package com.manama.wishlist.core.models

data class WishListItem(
    val itemId: String,
    val name: String,
    val amount: Int,
    val quantity: Int,
    val category: String,
    val priority: String,
    val createdAt: String,
    val imageUrl: String,
    val isPurchased: Boolean = false,
) {
    fun doesMatchSearchQuery(query: String): Boolean {
        val matchingCombinations = listOf(name)

        return matchingCombinations.any {
            it.contains(query, ignoreCase = true)
        }
    }
}