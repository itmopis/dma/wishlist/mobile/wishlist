
package com.manama.wishlist.core.datastore.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.manama.wishlist.core.util.Constants
import com.manama.wishlist.core.util.Constants.IS_FIRST_TIME_LAUNCH
import com.manama.wishlist.core.util.Constants.THEME_OPTIONS
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class UserPreferences @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    fun getIsFirstTimeLaunch(): Flow<Boolean> {
        return dataStore.data.map { preferences ->
            preferences[IS_FIRST_TIME_LAUNCH] ?: true
        }
    }

    suspend fun setIsFirstTimeLaunch() {
        dataStore.edit { preferences ->
            preferences[IS_FIRST_TIME_LAUNCH] = false
        }
    }

    suspend fun setTheme(themeValue: String) {
        dataStore.edit { preferences ->
            preferences[THEME_OPTIONS] = themeValue
        }
    }

    fun getTheme(): Flow<String> {
        return dataStore.data.map { preferences ->
            preferences[THEME_OPTIONS] ?: Constants.LIGHT_MODE
        }
    }

}