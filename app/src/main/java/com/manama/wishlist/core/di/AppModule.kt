
package com.manama.wishlist.core.di

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.room.Room
import com.manama.wishlist.core.datastore.preferences.UserPreferences
import com.manama.wishlist.core.room.database.WishlistAppDatabase
import com.manama.wishlist.core.util.Constants
import com.manama.wishlist.data.UserPreferenceRepositoryImpl
import com.manama.wishlist.data.WishlistRepositoryImpl
import com.manama.wishlist.domain.repository.UserPreferenceRepository
import com.manama.wishlist.domain.repository.WishlistRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideWishListAppDatabase(
        app: Application
    ): WishlistAppDatabase {
        return Room.databaseBuilder(
            app,
            WishlistAppDatabase::class.java,
            Constants.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideDatastorePreferences(@ApplicationContext context: Context):
            DataStore<Preferences> {
        return PreferenceDataStoreFactory.create(
            produceFile = {
                context.preferencesDataStoreFile(name = Constants.USER_PREFERENCES)
            }
        )
    }

    @Provides
    @Singleton
    fun provideUserPreferences(dataStore: DataStore<Preferences>): com.manama.wishlist.core.datastore.preferences.UserPreferences {
        return com.manama.wishlist.core.datastore.preferences.UserPreferences(dataStore = dataStore)
    }

    @Provides
    @Singleton
    fun provideUserPreferencesRepository(userPreferences: com.manama.wishlist.core.datastore.preferences.UserPreferences):
            UserPreferenceRepository {
        return UserPreferenceRepositoryImpl(
            preferences = userPreferences
        )
    }

    @Provides
    @Singleton
    fun provideWishListItemRepository(db: WishlistAppDatabase):
            WishlistRepository {
        return WishlistRepositoryImpl(db = db)
    }


}