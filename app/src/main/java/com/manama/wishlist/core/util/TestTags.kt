
package com.manama.wishlist.core.util

object TestTags {
    const val HOME_SCREEN_HEADER = "HOME_SCREEN_HEADER"
    const val HOME_SCREEN_SEARCH_BAR = "HOME_SCREEN_SEARCH_BAR"
    const val HOME_SCREEN_ADD_ITEM_ICON = "HOME_SCREEN_ADD_ITEM_ICON"
}