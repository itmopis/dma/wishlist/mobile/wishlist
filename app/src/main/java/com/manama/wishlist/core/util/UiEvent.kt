
package com.manama.wishlist.core.util

abstract class Event

sealed class UiEvent : Event() {
    data class ShowSnackbar(val uiText: String) : UiEvent()
    data class Navigate(val route: String) : UiEvent()

    data class CloseBottomSheet(val bottomSheets: BottomSheets) : UiEvent()


}