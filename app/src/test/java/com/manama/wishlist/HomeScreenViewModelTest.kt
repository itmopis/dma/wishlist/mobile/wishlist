
package com.manama.wishlist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.manama.wishlist.core.room.entities.WishlistItemEntity
import com.manama.wishlist.domain.repository.WishlistRepository
import com.manama.wishlist.ui.screens.home.HomeScreenViewModel
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class HomeScreenViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var subject: HomeScreenViewModel

    private var mockRepo = mockk<WishlistRepository>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        val mockItems = mockk<Flow<List<WishlistItemEntity>>>()
        every { mockRepo.getAllWishlistItems() } returns mockItems
        subject = HomeScreenViewModel(repository = mockRepo)


    }


    @Test
    fun `Verify wishlist items are fetch on instantiation`() = runTest {
        subject

        verify { mockRepo.getAllWishlistItems() }

    }
}